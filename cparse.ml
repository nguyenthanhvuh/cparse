open Printf
open Cil
open Utils
open Cil_common


let rec block_analysis b = 
  let bstmts = match b.bstmts with 
    | [] -> "None"
    | [x] -> stmt_analysis x
    |_ -> sprintf "[%s]" (S.concat ";" (L.map (stmt_analysis) b.bstmts))
  in
  bstmts


and lval_analysis lval = 
  let(lh,os) = lval in 
  match lh with
    |Var varinfo -> varinfo.vname
    |_ -> "other lval"

and exp_analysis exp = 
  match exp with
    |Const c -> get_const c

    |Lval lval -> lval_analysis lval

    |BinOp (bop,e1,e2,typ) -> 
      sprintf "%s %s %s" (exp_analysis e1) (get_binop bop) (exp_analysis e2)
      
    |_ -> "other exp"


and instr_analysis instr =
  match instr with
    |Set(lv,e,loc) -> sprintf "Assignment(%s,%s)" (lval_analysis lv) (exp_analysis e)

    |Call(lv_opt,e,es,loc) ->
      let lv' = match lv_opt with
        |None -> "None"
        |Some lv -> "(" ^ lval_analysis  lv ^ ")" in
      let es' = match es with
        |[] -> "[]"
        |_ -> S.concat ";" (L.map exp_analysis es) in

           "Call(" ^ lv' ^ "," ^
                 exp_analysis  e  ^ "," ^ es' ^ "," ^
                 get_loc loc ^ ")"

    |_ ->  "other instrs"

      
and stmtkind_analysis sk = 
  match sk with 
    | If(e,b1,b2,_) ->
      let b1' = block_analysis b1 in
      let b2' = block_analysis b2 in 
      sprintf "Conditional(FExp(%s),%s,%s)" 
	(exp_analysis e)  b1' b2'

    | Loop(b,loc,s1,s2) -> 
      let s1' = match s1 with
        |None -> "None"  |Some s -> stmt_analysis s in
      let s2' = match s2 with
        |None -> "None"  |Some s -> stmt_analysis s in
      sprintf "While(%s,%s,%s)" (block_analysis b) s1' s2'

    | Instr il ->   
      let il' = match il with
        |[] -> "[]"
        |_ -> sprintf "[%s]" (S.concat "," (L.map instr_analysis il))
      in
      il'

    | Return (e,loc) -> "#return\n"

    | Switch(e,b,sl,loc) -> "#switch"

    | Break loc -> "#break\n"
    | Continue loc -> "#continue\n"

    | Block b -> "#block\n"

    | _ -> "other stmtkind"
	
and stmt_analysis s = stmtkind_analysis s.skind

class traverseVisitor () = object
  inherit nopCilVisitor

  method vstmt s = 
    let res = stmt_analysis s in 
    printf "%s\n" (res);
    SkipChildren

  method vvdec varinfo = 
    (*printf "#obtain variable information\n";*)
    SkipChildren

end


let main () = begin 
  if Array.length Sys.argv < 2 then begin
    printf "usage: vu [file.c]\n" ;
    exit 1 
  end ;
  Cil.initCIL () ; 

  let filename = Sys.argv.(1) in 
  let ast = Frontc.parse filename () in 
  let traverse_v = (new traverseVisitor()) in 
  visitCilFileSameGlobals traverse_v ast;

  
  exit 0 

end 
;;
main () ;; 

