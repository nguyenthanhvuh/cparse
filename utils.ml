open Printf
module L = List 
module D = Digest
module H = Hashtbl 
module R = Random 
module S = String 
module A = Array 
module P = Printf
module U = Unix
module M = Marshal

let soi = string_of_int 
and sof = string_of_float 
and sob = string_of_bool 
and foi = float_of_int
and fos = float_of_string
and iof = int_of_float
and ios = int_of_string

(*Debug level, 0 is None/Clean output*)
let dbl= ref 3 and db_dir = ref "/tmp/jjj"

(*** Miscs functions ***)
let id e = e
let cx f (e1,e2) = (f e1, f e2) (*apply f to element in tuple*)
let compose l e = L.fold_left (fun a f -> f a ) e (L.rev l)
let map_tl f l = L.rev (L.rev_map f l)

let mysort ?(option="incr") l  = (
  let res = L.sort(fun(a,_)(b,_)->compare a b) l in 
  if option = "incr" then res else L.rev res
)

let rec drop n = function _::l when n > 0 -> drop (pred n) l | l -> l
let rec take n =  function h::t when n > 0 -> h::take (pred n) t |_ -> []

let rec range i j = if i >= j then [] else i :: (range (i+1) j)

let incr_vn1 a = let t = !a in incr a ; t
and incr_vn2 a = incr a ; !a

let incr_array2 a i v = a.(i)<- a.(i)+v  (*incr by value v*)
let incr_array1 a i = incr_array2 a i 1     (*incr by 1*)

(* return a copy of 'lst' where each element occurs once *) 
let uniq l = 
  let ht = H.create (L.length l) in 
  let l = L.filter (
	fun e -> if H.mem ht e then false else (H.add ht e (); true )
  ) l in l

(*** I/O functions ***)
let dg_out:out_channel ref = ref stdout 
let write_dg fmt = let k res = (output_string !dg_out res) in kprintf k fmt

let debug_out:out_channel ref = ref stderr
let debug fmt = 
  let k res = (	
	output_string stdout res; flush stdout;
	if not (!debug_out = stderr) then output_string !debug_out res
  ) in kprintf k fmt 


let myassert s b = if not b then (debug "%s" s; exit 1)

let file_size f = try let stats = U.stat f in stats.U.st_size with _ -> 0

let check_file ?(dtls=false) f = 
  if (Sys.file_exists f) then (
	if dtls then debug "File %s .. OK\n" f; true
  ) 
  else (
	if dtls then debug "File %s .. not exist!\n" f; false
  )

let delete_file ?(dtls=false) f = 
  try U.unlink f; true with e -> if dtls then debug "Cannot delete file %s\n" f; false
  
let count_lines f  = 
  try let f_in = open_in f and numL = ref 0 in
  (try while true do  ignore (input_line f_in); incr numL done; 0.
   with _ -> (close_in f_in; foi !numL))
  with _ -> 0.

let copy_obj (x : 'a) = let s = M.to_string x [] in (M.from_string s 0 : 'a) 

let rmdir s = 
  let rm_cmd = sprintf "rm -rf %s" s in match U.system rm_cmd with 
	|U.WEXITED(0) -> () |_ -> debug "cannot execute %s\n" rm_cmd
  
(*** String functions ***)
let vn_sub_str s1 s2 :bool= 
  try ignore(Str.search_forward (Str.regexp s1) s2 0); true 
  with Not_found -> false
let vn_split ss = Str.split (Str.regexp_string ss) 
let vn_split_ws = vn_split " "
let vn_split_trim ss str = 
  L.filter (fun s -> not(compare s ss = 0)) (vn_split_ws str)
let vn_trim_ws s= L.fold_left (^) "" (vn_split_trim "" s) 

let parse_options_in_file file :unit = 
  let args = ref [Sys.argv.(0)] in 
  try
	let fin = open_in file in
	try while true do
	  let line = input_line fin in 
	  let words = Str.bounded_split (Str.regexp "[ \t]+" ) line 2 in 
	  args := !args @words
	done with _->close_in fin
  with _ -> ()


(*** Probability functions ***)

(* Returns the elements of l in a random order. *) 
let rand_order l = (
  let a = L.map (fun x -> R.float 1.0, x) l in
  let b = mysort a in 
  L.map snd b 
)
let prob = function 
	p when p <= 0.0 -> false 
  | p when p >= 1.0 -> true 
  | p -> R.float 1.0 <= p

let printlist s = L.iter (debug s)
and printarray s = A.iter (debug s)
and printhash s = H.iter (debug s)
and printpath s = L.iter(fun(a,b) -> debug s a b)

let get_list_from_ht e ht = if H.mem ht e then H.find_all ht e else []

(* [e0,e0,e0]returns["0 e0","1 e1"; "2 e2"] *)
let getlist_idx s l = 
  fst (L.fold_left (fun (l',i) e -> (l'@[(sprintf s i e)],succ(i))) ([],0) l)

let print_rand_num seed s = 
  debug "%s(seed %d [" s seed; 
  L.iter(fun _ -> debug "%g " (R.float 1.0)) (range 0 4);
  debug "])\n" 

let print_list_ast l h = (
  let rl = range 0 (L.length l) in 
  L.iter2 (
	fun x sid -> (debug "num %d, sid %d\n%s\n\n" x sid (H.find h sid))
  ) rl l 
)

(*** STATISTICS ***)

let sum = L.fold_left (+.) 0.
let sum_l l = L.fold_left (fun a (s,_) -> a +. s ) 0.0 l

let sqr x = x *. x 
let mean l =  sum l /. foi (L.length l);;
let stdv l = let m = mean l in sqrt (mean (L.map (fun x -> sqr (x -. m))l))

let covariance xs ys = (
  let f m = L.map (fun x -> x -. m)  in
  let xs_m, ys_m = mean xs, mean ys in 
  let xs_l, ys_l = (f xs_m) xs ,(f ys_m) ys in
  let cov = mean (L.map2 ( *. ) xs_l ys_l) in
  cov
)

let correlation xs ys = (covariance xs ys) /. ((stdv xs) *. (stdv ys))

let get_correl xs ys s= (
  debug "%s\n" s;
  debug "opt (dist)= ["; printlist "%4g" xs; debug "]\n"; 
  debug "fitness   = ["; printlist "%4g" ys ; debug "]\n";
  debug "covariance= %g,   correlation_R= %g\n\n" 
	(covariance xs ys) (correlation xs ys)
)


(*** Conversion functions ***)

let my_int_of_string s = (
  try Scanf.sscanf s " %i" id  
  with _ -> (
	match S.lowercase s with 
	  | "true" -> 1  | "false" ->  0 
	  | _ -> failwith ("cannot convert to an integer: " ^ s)
  )
)

let my_float_of_string s = (
  try Scanf.sscanf s " %g" id
  with _ -> failwith ("cannot convert to an float: " ^ s)
)
(*** TIME STATS ***)
let delay secs = (
  let end_t = U.gettimeofday() +. secs in 
  while (U.gettimeofday() < end_t) do () done
)

let pause() = output_string stdout "press any key to continue"; flush stdout; ignore(input_line stdin)

let get_etime start_t = U.gettimeofday() -. start_t

type time_stat = {tname:string; mutable calls: int; mutable total_t:float}

let func_ht:(string,time_stat)H.t = H.create 255
let time name f a = (
  let start_t = U.gettimeofday() in 
  let finish() = (
	let run_t = U.gettimeofday() -. start_t in
	if H.mem func_ht name then (
	  let trecord = H.find func_ht name in
	  trecord.calls <- trecord.calls + 1;
	  trecord.total_t <- trecord.total_t +. run_t;
	) else (
	  let trecord = {tname = name ; calls = 1; total_t = run_t;} in
	  H.replace func_ht name trecord ;
	) 
  )in 
  try let res = f a in finish(); res with e -> finish(); raise e
)

let print_time_stat() = (
  H.iter(
	fun name ts -> debug "Name %-15s calls %8d time %7.2f\n" 
	  ("\""^ts.tname^"\"") ts.calls ts.total_t 
  ) func_ht 
)
