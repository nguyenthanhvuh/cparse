open Cil
open Printf
open Utils
open Check



(* dn_stmt does not print the #line directive, whereas d_stmt does *)
let cilPrint' f s :string = Pretty.sprint ~width:80 (f () s)
let cilPrint ?(dtls=false) g h s :string = cilPrint' (if dtls then g else h) s

let get_type ?(dtls=false) = cilPrint ~dtls d_type dn_type 
let get_exp ?(dtls=false) = cilPrint ~dtls d_exp dn_exp
let get_lval ?(dtls=false) = cilPrint ~dtls d_lval dn_lval
let get_attr ?(dtls=false) = cilPrint ~dtls d_attr dn_attr
let get_attrparam ?(dtls=false) = cilPrint ~dtls d_attrparam dn_attrparam
let get_attrlist ?(dtls=false) = 
  function []-> "[]" | l ->cilPrint ~dtls d_attrlist dn_attrlist l
let get_stmt ?(dtls=false) = cilPrint ~dtls d_stmt dn_stmt
let get_stmtkind ?(dtls=false) (sk:stmtkind) = get_stmt ~dtls (mkStmt sk)
let get_instr ?(dtls=false) = cilPrint ~dtls d_instr dn_instr
let get_type ?(dtls=false) = cilPrint ~dtls d_type dn_type
let get_ikind ?(dtls=false) = cilPrint' d_ikind
let get_const ?(dtls=false) = cilPrint' d_const
let get_loc ?(dtls=false) = cilPrint'  d_loc 
let get_block ?(dtls=false) = cilPrint' d_block
let get_label ?(dtls=false) = cilPrint' d_label
let get_binop ?(dtls=false) = cilPrint' d_binop
let get_unop ?(dtls=false) = cilPrint' d_unop
let rec get_offset ?(dtls=false) os = 
  let get_dtls() = 	match os with 
	|NoOffset -> "NoOffset"
	|Field(fieldinfo,os') -> 
	   "Field(" ^"," ^ "fieldinfo" ^ "," ^ get_offset ~dtls os' ^ ")"
	|Index(exp,os') -> 
	   "Index(" ^  get_exp ~dtls exp ^  "," ^ (get_offset ~dtls os') ^ ")"
  in 
  if dtls then get_dtls() else cilPrint' (d_offset Pretty.nil) os 
	
let get_varinfo ?(dtls=false) v = sprintf "vname=%s, vtype=%s, vattrs=%s" 
  v.vname (get_type v.vtype) (get_attrlist v.vattr)
let get_fundec ?(dtls=false) f = 
  sprintf "var: %s\nslocals:\n\t%s\nblock:%s" 
	(get_varinfo ~dtls f.svar) 
	(S.concat "\n\t+ " (L.map (get_varinfo ~dtls) f.slocals))
	(get_block f.sbody)
	

let rec typeOfAddr exp = 
  match exp with 
	|StartOf lv -> typeOfLval lv 
	|CastE(_,e) -> typeOfAddr e
	| _ -> typeOf exp 

let stripCastFrom d = 
  (* if d is of the form (type)d then we strip typeT out from d 
	 so that we can do things like sizeof(d) instead sizeof((typeT)d)  
	 e.g.,  char d[10] = ""  ;  
	 sizeof(d)=10 but sizeof((char \* ) d) = 8 *)
  match d with CastE(_,e) -> e |_ -> d


let writeFile ?(use_stdout=false) fname ast = 
  let df o = dumpFile defaultCilPrinter o fname ast in 
  if use_stdout then df stdout else let o = open_out fname in df o; close_out o

let writeFile_bin ?(bin=true) fname ast = 
  let o = (if bin then open_out_bin else open_out) fname in 
  M.to_channel o ast []; close_out o

let readFile ?(bin=true) fname = 
  let i = (if bin then open_in_bin else open_in) fname in 
  let res = M.from_channel i in close_in i; res
